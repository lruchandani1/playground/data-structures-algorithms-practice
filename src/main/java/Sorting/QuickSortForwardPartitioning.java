package Sorting;

import commons.Utils;

public class QuickSortForwardPartitioning {

    private static int[] quickSort(int[] array){
        return  quickSort(array,0,array.length-1);
    }

    private static int[] quickSort(int[] array,int start, int end){
        if(start<end){
            int pivotPosition = partition(array,start,end);
            quickSort(array,start,pivotPosition);
            quickSort(array,pivotPosition+1,end);
        }
        return array;
    }

    private static int partition(int[] array, int start, int end) {
        int pivot = array[start];
        int partitionBoundary = start +1;
        for(int currentValIndex = partitionBoundary ; currentValIndex<=end;currentValIndex++){
            if(array[currentValIndex]<pivot){
                Utils.swap(array,partitionBoundary,currentValIndex);
                partitionBoundary++;
            }
        }
        Utils.swap(array,start,partitionBoundary-1);
        return partitionBoundary -1;
    }

    public static void main(String[] args) {
        int[] numbers = Utils.takeIntArray();
        Utils.print(quickSort(numbers));
    }
}
