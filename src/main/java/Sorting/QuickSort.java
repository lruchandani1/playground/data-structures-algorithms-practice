package Sorting;

import commons.Utils;

import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.IntStream;

import static java.util.Arrays.copyOf;

public class QuickSort {


    public static int[] selectionSort(int[] unsorted){
        int[] array = copyOf(unsorted,unsorted.length);
        for(int i=0;i<array.length;i++){
            for(int j=i+1;j<array.length;j++){
                if(array[i]>array[j]){
                    int temp = array[i];
                    array[i]=array[j];
                    array[j]=temp;
                }
            }
        }
        return array;
    }

    static  int[] bubbleSort(int[] unsorted){
        int[] array = copyOf(unsorted,unsorted.length);
        for(int i=array.length-1;i>=0;i--){
            for(int j=i;j>=0;j--){
                if(array[i]<array[j]){
                    int temp = array[i];
                    array[i]=array[j];
                    array[j]=temp;
                }
            }
        }
        return array;
    }

    public static void main(String[] args){
        System.out.println("Selection sort : 1");
        System.out.println("Bubble Sort : 2");
        System.out.println("Insertion Sort: 3");
        System.out.println("Quick Sort : 4");
        System.out.printf(" Enter selection :");
        Scanner scanner = new Scanner(System.in);
        int selection = scanner.nextInt();
        switch (selection){
            case 1: print(selectionSort(Utils.takeIntArray())); break;
            case 2: print(bubbleSort(Utils.takeIntArray()));break;
            case 3: print(insertionSort(Utils.takeIntArray()));break;
            case 4 : print(quickSort(Utils.takeIntArray()));break;
            default: System.err.printf("Invalid selection");
        }
    }

    private static int[] insertionSort(int[] arr) {
        for(int i=1;i<arr.length;i++){
            int j=i;
            int val = arr[i];
            //move elements greater than current val
            // to the right to make space for insertion of val at correct position - lower bound(beginning of array i.e. 0)
            while(j>0 && arr[j-1]>val){
                arr[j]=arr[j-1];//copy the previous value to the current(moving right)
                j--;
            }
            arr[j]=val; //do the insertion of the current val(which a[i])
        }
        return arr;
    }


    private static int[] quickSort(int[] numbers) {
        if(numbers.length==1)
            return numbers;
        int low = 0;
        int high = numbers.length-1;
        return quickSort(numbers,low,high);
    }

    public static int[] quickSort(int[] numbers, int low, int high){
           if(low<high){
                int pivot = partition(numbers,low,high);
                quickSort(numbers,low,pivot-1);
                quickSort(numbers,pivot+1,high);
            }
            return numbers;
    }


    public static int partition(int[] unsorted, int low , int high){
        int pivotNum = unsorted[high];
        int leftPos = low , rightPos=high-1;
           if(low<high){
                while (leftPos<rightPos){
                    if(unsorted[leftPos]>pivotNum){
                        swap(unsorted,leftPos,rightPos);
                        rightPos--;
                    } else {
                        leftPos++;
                    }
                }
                swap(unsorted,rightPos,high);
            }
            return leftPos;
    }



    private static void swap(int[] array, int leftPos, int rightPos) {
        int temp = array[rightPos];
        array[rightPos]=array[leftPos];
        array[leftPos]=temp;
    }

    static  void print(int[] array){
        IntStream.of(array).mapToObj(Integer::toString).map(s -> s+ " ").forEach(num -> System.out.print(num));
        System.out.println();
    }


}
