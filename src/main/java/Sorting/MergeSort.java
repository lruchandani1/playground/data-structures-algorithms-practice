package Sorting;

import commons.Utils;

/**
 * Merge sort works on dive and conquer approach, following is algortihm
 * 1) Divide the array until it cannot be divided further
 * 2) Merge the resultant by comparing the first element of the array
 */
public class MergeSort {

    public int[] mergeSort(int[] array){
        int[] sortedArray= new int[array.length];
        mergeSort(sortedArray,array,0,array.length-1);
        return sortedArray;
    }

    public  void mergeSort(int[] sortedArray,int[] array, int start, int end){
        if (end - start > 0) {
            int mid = (start + end) / 2;
            mergeSort(sortedArray, array, start, mid);
            mergeSort(sortedArray, array, mid + 1, end);
            merge(sortedArray, array, start, mid, end);
        }
    }

    private  void merge(int[] sortArray,int[] array, int start, int mid, int end) {
        int leftPartitionStartPosition = start;
        int rightPartitionStartPosition = mid+1;
        int sortArrayCurrentPosition = start;
        for(int currentIndex=start;currentIndex<=end;currentIndex++){
            if(leftPartitionStartPosition>mid){ //merged all the items from left parition, so remaing are all right ones that have to be merged
                sortArray[sortArrayCurrentPosition++]=array[rightPartitionStartPosition++];
            }else if(rightPartitionStartPosition>end){
                sortArray[sortArrayCurrentPosition++] = array[leftPartitionStartPosition++];
            } else if(array[leftPartitionStartPosition]>array[rightPartitionStartPosition]){
                sortArray[sortArrayCurrentPosition++] = array[rightPartitionStartPosition++];
            }else{
                sortArray[sortArrayCurrentPosition++] = array[leftPartitionStartPosition++];
            }
        }
        Utils.print(sortArray);
        System.arraycopy(sortArray,start,array,start,end-start+1);
    }

    public static void main(String[] args) {
        int[] numbers = Utils.takeIntArray();
        MergeSort mergeSort = new MergeSort();
        Utils.print(mergeSort.mergeSort(numbers));
    }

}
