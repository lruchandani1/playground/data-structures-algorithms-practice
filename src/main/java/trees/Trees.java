package trees;

import commons.Utils;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Trees {

  public static void main(String[] args) {
    System.out.println("Problem on trees =============:");
    System.out.println("Create binary tree : 1");
    System.out.println("In-order traversal: 2");
    System.out.println("Pre-Order : 3");
    System.out.println("Post order : 4");
    System.out.println("Level order : 5");
    System.out.println(" Find Max element in tree :6");
    System.out.println("Enter Choice:");
    Scanner scanner = new Scanner(System.in);
    int choice = scanner.nextInt();
    switch (choice){
      case 1: createTree(); break;
      case 2:
      case 3:
      case 4:
      case 5:
      case 6 :
        default:
    }
  }

  private static void createTree() {
    int[] input = Utils.takeIntArray();
    Node head = null;
    for(int i=0;i<input.length;i++){
      if(head==null){
        head = new Node();
        head.number = input[i];
      }else {
        Node start = head;
        while(true){
          if(input[i]>=start.number) {
            if (start.right != null) {
              start = start.right;
            } else {
              start.right = new Node();
              start.right.number = input[i];
              break;

            }
          }else {
            if(start.left!=null){
              start=start.left;
            }else {
              start.left = new Node();
              start.left.number = input[i];
              break;
            }
          }
        }
      }
    }
    printTree(head);
    printLevelOrder(head);
  }

  public static void printTree(Node head){
    if(head!=null){
      System.out.printf("%d",head.number);
      printTree(head.left);
      printTree(head.right);
    }
  }

  public static void printLevelOrder(Node head){
    Queue<Node> queue = new LinkedList<>();
    if(head!=null){
      queue.add(head);
      while(!queue.isEmpty()){
        Node start = queue.poll();
        System.out.printf("%d ",start.number);
        if(start.left!=null){
          queue.add(start.left);
        }
        if(start.right!=null){
          queue.add(start.right);
        }
      }
    }
  }

}
