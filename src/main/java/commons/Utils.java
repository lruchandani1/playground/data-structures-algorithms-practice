package commons;

import java.util.Scanner;
import java.util.stream.IntStream;

public class Utils {

    public static int takeInt(){
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

    public static int[] takeIntArray(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter no of elements required :");
        Integer arraySize =  scanner.nextInt();
        System.out.println("Enter  comma separated numbers : ");
        int[] numbers = new int[arraySize];
        for(int i=0;i<arraySize;i++){
            numbers[i]=scanner.nextInt();
        }
        scanner.close();
        return numbers;
    }

    public static void swap(int[] array, int leftPos, int rightPos) {
        int temp = array[rightPos];
        array[rightPos]=array[leftPos];
        array[leftPos]=temp;
    }

    public static  void print(int[] array){
        IntStream.of(array).mapToObj(Integer::toString).map(s -> s+ " ").forEach(num -> System.out.print(num));
        System.out.println();
    }

    public static void print(String formattedStr,int...args){
        System.out.printf(formattedStr,args);
    }
}
