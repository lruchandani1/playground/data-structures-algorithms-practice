package graphs;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class GraphsDfsBfs {

  static interface StateCollector<E> {
    boolean push(E element);
    E pull();
    boolean isEmpty();
  }

  /**
   *
   */
  static class Queue implements  StateCollector<Integer>{
    java.util.Queue<Integer> delegate;
    public Queue(java.util.Queue<Integer> delegate){
      this.delegate = delegate;
    }
    @Override
    public boolean push(Integer number) {
      return delegate.add(number);
    }

    @Override
    public Integer pull() {
      return delegate.poll();
    }

    @Override
    public boolean isEmpty() {
      return delegate.isEmpty();
    }
  }

  /**
   *
   */
  static class Stack implements  StateCollector<Integer>{
    java.util.Stack<Integer> delegate;
    public Stack(java.util.Stack<Integer> delegate){
      this.delegate=delegate;
    }
    @Override
    public boolean push(Integer element) {
       return delegate.add(element);
    }

    @Override
    public Integer pull() {
      return delegate.pop();
    }

    @Override
    public boolean isEmpty() {
      return delegate.isEmpty();
    }
  }

  /**
   *
   */
  static class Graph {

    int numOfNodes;
    List<Integer>[] V;

    public Graph(int numOfNodes) {
      this.numOfNodes = numOfNodes + 1; //offset for 1
      V = new ArrayList[this.numOfNodes];
    }

    public void addEdge(int v, int u) {
      if (this.V[v] == null) {
        this.V[v] = new ArrayList<>();
      }
      if (this.V[u] == null) {
        this.V[u] = new ArrayList<Integer>();
      }
      this.V[v].add(u);
      this.V[u].add(v);
    }

    public void print(int val, StateCollector<Integer> stateCollector) {
      boolean[] visited = new boolean[this.numOfNodes];
      if (this.V == null || this.numOfNodes == 0) {
        System.out.println("Empty Graph");
      }
      stateCollector.push(val);
      while (!stateCollector.isEmpty()) {
        //take the number out
        int num = stateCollector.pull();
        if(!visited[num]) {
          System.out.printf("%d ", num);
          visited[num] = true;
        }
        //suspend the number and explore further
        for (int i = 0; i < V[num].size(); i++) {
          int neighbour = V[num].get(i);
          if (!visited[neighbour]) {
            stateCollector.push(neighbour);
          }
        }
      }
      System.out.println("");
    }

    public void shortestPathBetweenTwoNodes(int source, int destination,
        StateCollector<Integer> stateCollector) {
      boolean[] visited = new boolean[this.numOfNodes];
      int[] distance = new int[this.numOfNodes];
      if (this.V == null || this.numOfNodes == 0) {
        System.out.println("Empty Graph");
      }
      stateCollector.push(source);
      distance[source] = 0;
      while (!stateCollector.isEmpty()) {
        //take the number out
        int num = stateCollector.pull();
        visited[num] = true;
        //suspend the number and explore further
        for (int i = 0; i < V[num].size(); i++) {
          int neighbour = V[num].get(i);
          if (!visited[neighbour]) {
            stateCollector.push(neighbour);
            distance[neighbour] = distance[num] + 1;
          }
          if (neighbour == destination) {
            System.out.printf("Shortest path from %d to %d is %d", source, destination,
                distance[destination]);
            System.exit(1);
          }
        }
      }
      System.out.printf("No path exist between %d and %d", source, destination);
    }
  }
    public static void main(String[] a) {
      Scanner scanner = new Scanner(System.in);
      System.out.printf("Enter number of nodes :");
      int num = scanner.nextInt();
      System.out.printf("Number of Edges:");
      int edges = scanner.nextInt();
      scanner.nextLine();
      Graph graph = new Graph(num);
      while (edges > 0) {
        Scanner lineTokenizer = new Scanner(scanner.nextLine());
        int nodeA = lineTokenizer.nextInt();
        int nodeB = lineTokenizer.nextInt();
        graph.addEdge(nodeA, nodeB);
        lineTokenizer.close();
        edges--;
      }
      System.out.printf("Number to print from :");
      int number = scanner.nextInt();

      //printing
      Queue queue = new Queue(new LinkedList<>());
      Stack stack = new Stack(new java.util.Stack<>());
      System.out.println("---BF Traversal---");
      graph.print(number, queue); //BFS
      System.out.println("---DF Traversal---");
      graph.print(number, stack); //DFS
      System.out.printf("Find path - Enter source node: ");
      int source = scanner.nextInt();
      System.out.printf("Find path - Enter destination: ");
      int destination = scanner.nextInt();
      graph.shortestPathBetweenTwoNodes(source,destination,new Queue(new LinkedList<>()));
    }
}
