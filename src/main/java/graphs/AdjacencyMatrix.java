package graphs;

import commons.Utils;

import java.util.Scanner;

/**
 * Find number of edges to be removed to get exactly k connected nodes
 * You are given a graph with  m nodes and  n edges.
 * Calculate maximum number of edges that can be removed from the graph so that it contains exactly  connected components.
 *
 *
 * Input
 *
 * The first line contains m,n, k (in order).
 * The next m lines have  number, and  that showS there is an edge between those nodes.
 * It is guaranteed that input is valid(no multiple edges and no loops).
 * example
 * 4 3 2
 * 1 2
 * 2 3
 * 1 3
 *
 * Output
 * 1
 * Maximum number of edges that can be removed from the graph such that it contains exactly  connected components.
 * If the graph intially has more than  components print .
 */
public class AdjacencyMatrix {

  public static void main(String[] args) {
    System.out.println("Enter number of vertices :");
    int vertices = Utils.takeInt();
    System.out.println("Enter edges");
    int edges = Utils.takeInt();
    int [][] graph = new int[vertices+1][vertices+1];
    Scanner scanner = new Scanner(System.in);
    //build graph
    int currentlyConnectedNodes = 0;
    for(int i=0;i<edges;i++){
      int nodeA = scanner.nextInt();
      int nodeB = scanner.nextInt();
      if(nodeA>vertices || nodeB>vertices){
        System.err.println("Invalid input - the value cannot be greater than  the nodes allocated : "+vertices);
        System.exit(1);
      }
      graph[nodeA][nodeB]=1;
      graph[nodeB][nodeB]=1;
    }
  }


}
