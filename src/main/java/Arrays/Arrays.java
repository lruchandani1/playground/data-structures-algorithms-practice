package Arrays;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import commons.Utils;

public class Arrays {

  public static void main(String[] a){

    System.out.println("Select the program option from the below");
    print("max sub-array with even numbers",1);
    print("max sub-array with odd numbers",2);
    print("pair with sum equal to 10 ",3);
    print("max area between two bars ",4);
    print("sum of previous two numbers at the index",5);
    Scanner scanner = new Scanner(System.in);
    int option  = scanner.nextInt();
    switch (option){
      case 1:
        maxSubarray();
        break;
      case 2:
      case 3:
        pairWithSumEqual();
        break;
      case 4:
      case 5:
        sumPreviousTwoNumbers();
      default :
    }
  }

  private static void pairWithSumEqual() {
    int[] arr = Utils.takeIntArray();
    System.out.println("number who's pair has to be found :");
    Scanner scanner = new Scanner(System.in);
    int num = scanner.nextInt();
    int[] numberIndex = new int[11];
    boolean found= false;
    for(int i=0;i<arr.length;i++){
      int index = num - arr[i];
      if(numberIndex[index]>0){
        System.out.println(String.format("Pair for %d is %d and %d",num,arr[i],index));
        found=true;
        break;
      }else {
        numberIndex[arr[i]]++;
      }
    }
    if(!found){
      Utils.print("No pair found for number %d",num);
    }
  }

  private static void sumPreviousTwoNumbers() {
    int[] arr = Utils.takeIntArray();
    sum(arr,0);
    Utils.print(arr);
  }

  private static void sum(int[] arr,int n){
    if(n==arr.length-1 )
      return;
    arr[n+1]=arr[n+1]+arr[n];
    sum(arr,n+1);
  }

  private static void maxSubarray() {
    Scanner scanner = new Scanner(System.in);
    int[] input = Utils.takeIntArray();
    int arrayLen = input.length;
    Map<Integer, Integer> lengths = new HashMap<>();
    int maxPos = 0;
    int maxLen= 0;
    for(int index=0,start=0,end=0;index<arrayLen;index++){
      if(input[index]%2==0 ){
        end++;
      } else if(input[index]%2!=0 ){
        if((end-start)>maxLen){
          maxLen= end-start;
          maxPos=start;
        }
        start=index + 1;
      }

      if(index==arrayLen-1){
        if((end-start)>maxLen){
          maxLen= end-start;
          maxPos=start;
        }
      }

    }
    System.out.println(String.format("Max Subarray size is %d and its starting from %d",maxLen,maxPos));
  }

  public static void print(String name,int caseNumber){
    System.out.println(name + "-"+ caseNumber);
  }

}
