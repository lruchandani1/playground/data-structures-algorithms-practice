package stringmanipulation;

public class StringShifter implements Shifter {
    String source ;
    public StringShifter(String a) {
        super();
        source = a;
    }

    @Override
    public String shiftBy(int positions) {
        int stripBeginIndex = this.source.length()-positions;
        String stripedString =  this.source.substring(stripBeginIndex);
        return stripedString.concat(this.source.substring(0,stripBeginIndex));
    }
}
