package stringmanipulation;

public interface Shifter {
    public String shiftBy(int i);
}
