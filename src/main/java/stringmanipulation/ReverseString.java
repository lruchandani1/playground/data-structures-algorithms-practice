package stringmanipulation;

import java.util.Scanner;

public class ReverseString {

  public static void main(String[] a){
    Scanner scanner = new Scanner(System.in);
    System.out.printf("Enter String :");
    String s = scanner.nextLine();
    char[] c = s.toCharArray();
    for(int start=0,end=c.length-1;start<end;start++,end--){
      char temp = c[start];
      c[start]=c[end];
      c[end]=temp;
    }
    System.out.printf("reversed string :"+ new String(c));
  }

}
