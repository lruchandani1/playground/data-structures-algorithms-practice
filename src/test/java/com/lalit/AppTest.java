package com.lalit;

import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import stringmanipulation.Shifter;
import stringmanipulation.StringShifter;

/**
 * Unit test for simple App.
 */
public class AppTest 
{

    public Shifter stringShifter(String s){
        return new StringShifter(s);
    }


    public void assertEqual(String expected,String actual){
        Assert.assertTrue(expected.equalsIgnoreCase(actual));
    }

    @Test
    public void forStringOfOneCharlength(){
        Shifter stringShifter = new StringShifter("a");
        Assert.assertTrue("a".equalsIgnoreCase(stringShifter.shiftBy(1)));
    }

    @Test
    public void forStringOfTwoCharacters(){
        Shifter shifter= stringShifter("ab");
        Assert.assertTrue("ba".equalsIgnoreCase(shifter.shiftBy(1)));
    }

    @Test
    public  void forStringHacker(){
        Shifter shifter = stringShifter("hacker");
        assertEqual("erhack",shifter.shiftBy(2));

    }

}
